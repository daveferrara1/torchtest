const memfs = require('memfs');
const AdmZip = require('adm-zip')
const File = require('./lib/File');
const Directory = require('./lib/Directory');

// Create directory object.
const dir = new Directory();

// Create file objects.
const file1 = new File('.', 'foo.txt');
const file2 = new File('./src', 'bar.txt');
const file3 = new File('./src/files', 'foo-bar.js');

// Create file contents.
file1.addToFile('foo content');
file2.addToFile('bar content');
file3.addToFile('foo-bar content');

// Add files to directory.
dir.addFileToDirectory(file1.getFilePath(), file1.content);
dir.addFileToDirectory(file2.getFilePath(), file2.content);
dir.addFileToDirectory(file3.getFilePath(), file3.content);

// Create memory volume from object.
memfs.vol.fromJSON(dir, './');

// Create archive.
const zip = new AdmZip();

// Add all the files.
Object.keys(dir).forEach((filePath) => {
  const content = memfs.vol.readFileSync(filePath, 'utf8');
  zip.addFile(filePath, Buffer.alloc(content.length, content), null, 0);
})

// Save as zip.
const timestamp = Date.now();
const zipFilePath = `./files/zipped-file-${timestamp}.zip`;
zip.writeZip(zipFilePath);


//read that zip

//into memory
//add to it

// otput a new one.
