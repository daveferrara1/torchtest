/**
 * ES6 Class for making file objects.
 */
class File {
  constructor(path, fileName) {
    this.path = path;
    this.fileName = fileName;
  };

  /**
   * Add content to file.
   *
   * @param content
   */
  addToFile(content) {
    this.content = content;
  }

  /**
   * Returns a file path for the file.
   *
   * @returns {string}
   */
  getFilePath() {
    return `${this.path}/${this.fileName}`;
  }
}

module.exports = File;
