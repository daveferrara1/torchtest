/**
 * ES6 Class for making directory objects.
 */
class Directory {
  /**
   * Method adding a file to the directory.
   *
   * @param filePath
   * @param fileContents
   */
  addFileToDirectory(filePath, fileContents) {
    this[filePath] = fileContents;
  }
}

module.exports = Directory;
