Torchtest Application.

Simple application to demonstrate in memory file creation and building a zip archive of those files.

To run:

1. Git clone this repo to your machine.
2. Change to your project directory.
3. Run the following commands:
    1. `npm install`
    2. `node index.js`

When you run the app files will be created in memory and a zip archive of those files will be dumped into a directory named files.
